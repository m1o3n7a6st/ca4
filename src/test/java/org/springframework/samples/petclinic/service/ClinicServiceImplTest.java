package org.springframework.samples.petclinic.service;

import org.junit.After;
import org.junit.Before;
//import org.junit.Test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.samples.petclinic.model.*;
import org.springframework.samples.petclinic.repository.OwnerRepository;
import org.springframework.samples.petclinic.repository.PetRepository;

import java.text.SimpleDateFormat;
import java.util.*;
import com.github.mryf323.tractatus.ClauseCoverage;
import com.github.mryf323.tractatus.Valuation;
import org.junit.jupiter.api.Test;

@SpringBootTest
public class ClinicServiceImplTest {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    @Autowired
    private PetRepository petRepository;
    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private ClinicService ownerService;

    @Before
    public void setUp() throws Exception {
        Map<PetType, Boolean> petVetAvailabilityMap = new HashMap<>();
       PetType Bird =new PetType();
       Bird.setName("Bird");
       Bird.setId(1);
        PetType Dog =new PetType();
        Dog.setId(2);
        Dog.setName("dog");
        PetType Cat =new PetType();
        Cat.setId(3);
        Cat.setName("cat");
        petVetAvailabilityMap.put(Bird, true);
        petVetAvailabilityMap.put(Dog, false);
        petVetAvailabilityMap.put(Cat, true);

        Owner owner = new Owner();
        owner.setFirstName("Mona");
        owner.setLastName("davari");
        ownerRepository.save(owner);

        PetTest catPet1 = new PetTest();
        catPet1.setName("Cati");
        catPet1.setBirthDate(new Date( "2016/06/06"));
        Visit visit = new Visit();
        visit.setDate (new Date("2018/06/06"));
        Set<Visit> visitSet = new HashSet<>();
        visitSet.add(visit);
        catPet1.VisitsInternal(visitSet);
        catPet1.setOwner(owner);
        catPet1.setType(Bird);
        petRepository.save(catPet1);

        PetTest catPet2 = new PetTest();
        catPet2.setName("Cati2");
        catPet2.setBirthDate(new Date("2018/03/12"));
        Visit visit1 = new Visit();
        visit1.setDate(new Date ("2019/07/11"));
        Set<Visit> visitSet1 = new HashSet<>();
        visitSet1.add(visit1);
        catPet2.VisitsInternal(visitSet1);
        catPet2.setOwner(owner);
        catPet2.setType(Cat);
        petRepository.save(catPet2);

        PetTest dogPet = new PetTest();;
        dogPet.setName("Dogi");
        dogPet.setBirthDate(new Date("2015/07/06"));
        Visit visit2 = new Visit();
        visit.setDate (new Date("2019/01/06"));
        Set<Visit> visitSet2 = new HashSet<>();
        visitSet2.add(visit2);
        dogPet.VisitsInternal(visitSet2);
        dogPet.setOwner(owner);
        dogPet.setType(Dog);
        petRepository.save(dogPet);

        PetTest dogPet2 = new PetTest();
        dogPet2.setName("Dogi2");
        dogPet2.setBirthDate(new Date("2018/12/06"));
        Visit visit3 = new Visit();
        visit.setDate (new Date("2019/05/18"));
        Set<Visit> visitSet3 = new HashSet<>();
        visitSet3.add(visit3);
        dogPet2.VisitsInternal(visitSet3);
        dogPet2.setOwner(owner);
        dogPet2.setType(Dog);
        petRepository.save(dogPet2);

        PetTest birdPet = new PetTest();
        birdPet.setName("bird");
        birdPet.setBirthDate(new Date("2017/03/12"));
        Visit visit4 = new Visit();
        visit.setDate (new Date("2018/07/11"));
        Set<Visit> visitSet4 = new HashSet<>();
        visitSet4.add(visit4);
        birdPet.VisitsInternal(visitSet4);
        birdPet.setOwner(owner);
        birdPet.setType(Bird);
        petRepository.save(birdPet);

        PetTest birdPet2 = new PetTest();
        birdPet2.setName("bird2");
        birdPet2.setBirthDate(new Date("2018/12/06"));
        Visit visit5 = new Visit();
        visit.setDate (new Date("2019/05/18"));
        Set<Visit> visitSet5 = new HashSet<>();
        visitSet5.add(visit5);
        birdPet2.VisitsInternal(visitSet5);
        birdPet2.setOwner(owner);
        birdPet2.setType(Bird);
        petRepository.save(birdPet2);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void visitOwnerPets() {

    }
}
